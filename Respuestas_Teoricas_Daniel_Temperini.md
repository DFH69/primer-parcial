# Respuestas Teóricas
## a) Define muy brevemente los tres problemas principales del desarrollo de software que Docker ayuda a resolver.

Los tres problemas que Docker ayuda a resolver son, Construir, Destruir y Ejecutar Software.
* **Construir software de manera consistente:** Docker permite crear imágenes que contienen todas las dependencias y configuraciones necesarias para ejecutar una aplicación. Esto garantiza que el software se pueda construir de manera consistente en diferentes entornos y por diferentes desarrolladores. 
* **Distribuir software de manera fácil y eficiente:** Docker facilita la distribución de aplicaciones y servicios mediante la encapsulación de todo el entorno de ejecución en una imagen. Esto hace que sea más fácil compartir y desplegar aplicaciones en diferentes sistemas y plataformas. 
* **Ejecutar software de manera confiable y reproducible:** Docker proporciona un entorno aislado y reproducible para ejecutar aplicaciones, lo que ayuda a evitar problemas de dependencias y conflictos entre diferentes componentes de software. Esto garantiza que las aplicaciones se ejecuten de manera confiable en cualquier entorno donde Docker esté instalado. 

## b) ¿Qué comandos usarías para listar todos los contenedores en ejecución, los detenidos y con cuál pueden borrar de una sola vez, todos los contenedores que están detenidos?
Los comandos de Docker para listar todos los contenedores en ejecución, los detenidos y cómo eliminar todos los contenedores detenidos de una sola vez son los siguiente:

### Listar todos los contenedores en ejecución y detenidos:

> docker ps -a 

El comando ` docker ps -a ` lista todos los contenedores en el sistema, ya sea que estén en ejecución o detenidos. Aquí está lo que hace cada parte del comando:

**docker:** Este es el comando principal de Docker que se utiliza para interactuar con el motor de Docker.

**ps:** Esta es un subcomando de Docker que significa **"procesos"**. Usada en conjunto con otras opciones, permite listar contenedores.

**-a:** Esta opción significa "todos" y hace que el comando docker ps muestre todos los contenedores, incluyendo aquellos que están en ejecución y aquellos que están detenidos.

Entonces, en resumen, `docker ps -a` muestra una lista de todos los contenedores en el sistema, independientemente de si están en ejecución o detenidos.

### Eliminar todos los contenedores detenidos:

> docker container prune

Este comando eliminará todos los contenedores detenidos de una sola vez. Te pedirá confirmación antes de realizar la eliminación. Si estás seguro de que deseas eliminar todos los contenedores detenidos, puedes agregar la opción  `-f`  o `--force` para forzar la eliminación sin confirmación:

> docker container prune -f

Con estos comandos, se pueden administrar fácilmente los contenedores Docker, ya sea listando los contenedores en ejecución, detenidos o eliminando los contenedores detenidos de una sola vez.

# c) ¿Con tus palabras y en forma muy breve, cuál es la diferencia entre bind mounts y volúmenes en Docker?

Los bind mounts conectan un directorio del host con un contenedor, mientras que los volúmenes son entidades de almacenamiento independientes en Docker....